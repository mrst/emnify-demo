import Vue from 'vue';
import Vuex from 'vuex';
import { auth } from './modules/auth/auth';
import { endpoints } from './modules/endpoints/endpoints';

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    auth,
    endpoints,
  },

  state: {

  },

  mutations: {

  },

  actions: {

  },
});
