import Vue from 'vue';
import VuePaginate from 'vue-paginate';
import App from './App.vue';
import router from './router';
import store from './store';
import '@/assets/sprite';
import '@/components/base';

Vue.use(VuePaginate);

require('./startup/validations');

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app');
