import Vue from 'vue';
import Router from 'vue-router';
import SignLayout from '@/views/sign/SignLayout.vue';
import SignIn from '@/views/sign/SignIn.vue';
import AppEndpoints from '@/views/app/AppEndpoints.vue';
import AppLayout from './views/app/AppLayout.vue';

Vue.use(Router);

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/sign',
      name: 'sign',
      component: SignLayout,
      children: [
        {
          path: 'login',
          name: 'signIn',
          component: SignIn,
        },
      ],
    },
    {
      path: '/',
      component: AppLayout,
      redirect: '/endpoints',
      children: [
        {
          path: '/endpoints',
          name: 'endpoints',
          component: AppEndpoints,
        },
      ],
    },
  ],
});
