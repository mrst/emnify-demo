/**
 * Libs
 */
import _ from 'lodash';


/**
 * Mutations
 */
import {
  FETCH__ENDPOINTS_lIST,
} from './mutations';

/**
 * Services
 */
import { HTTP } from '../../services/axios-provider';
import { filterToQuery } from '../../services/filterToQuery';

export const endpoints = {
  namespaced: true,
  state: {
    list: null,
  },
  mutations: {
    [FETCH__ENDPOINTS_lIST](state, list) {
      state.list = list;
    },
  },
  actions: {
    // eslint-disable-next-line consistent-return
    async fetchList({ commit }, { queryParams, pagination }) {
      try {
        const query = _.isEmpty(queryParams) ? '' : filterToQuery(queryParams);

        const { data, headers } = await HTTP.get(`/endpoint?q=${query}${pagination}`);

        commit(FETCH__ENDPOINTS_lIST, data);

        return {
          headers,
          data,
        };
      } catch (error) {
        return {
          error,
        };
      }
    },
  },
  getters: {
    endpointsList: state => state.list,
  },
};
