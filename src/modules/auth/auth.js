/**
 * Libs
 */
import sha1 from 'js-sha1';

/**
 * Mutations
 */
import {
  SET_TOKEN, CHECK_STORAGE_TOKEN, LOGOUT,
} from './mutations';

/**
 * Services
 */
import { HTTP, setHeaders } from '../../services/axios-provider';

export const auth = {
  namespaced: true,
  state: {
    token: null,
  },
  mutations: {
    [CHECK_STORAGE_TOKEN](state) {
      const storageToken = localStorage.getItem('authToken');

      if (storageToken) {
        state.token = storageToken;

        setHeaders(storageToken);
      }
    },

    [LOGOUT](state) {
      state.token = null;
      state.userInfo = null;

      localStorage.removeItem('authToken');
    },

    [SET_TOKEN](state, token) {
      state.token = token;
    },
  },
  actions: {
    async login({ commit }, userInfo) {
      try {
        const { auth_token } = (await HTTP.post('/authenticate', {
          ...userInfo,
          password: sha1(userInfo.password),
        })).data;

        commit(SET_TOKEN, auth_token);

        localStorage.setItem('authToken', auth_token);
        setHeaders(auth_token);

        return { response: auth_token };
      } catch (error) {
        return { error: 'Wrong email or password' };
      }
    },
  },
  getters: {
    isAuth: state => state.token !== null,
  },
};
