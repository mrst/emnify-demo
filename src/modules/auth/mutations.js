export const SET_TOKEN = 'userLoginSuccess';
export const CHECK_STORAGE_TOKEN = 'checkStorageToken';
export const LOGOUT = 'logout';
