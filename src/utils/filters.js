export const STATUSES_OPTIONS_MAP = {
  enabled: 'Enabled',
  disabled: 'Disabled',
};

export const STATUSES_SORT_OPTIONS = {
  [STATUSES_OPTIONS_MAP.enabled]: 0,
  [STATUSES_OPTIONS_MAP.disabled]: 1,
};

export const STATUS_LABEL = {
  0: STATUSES_OPTIONS_MAP.enabled,
  1: STATUSES_OPTIONS_MAP.disabled,
};


export const initialFilters = {
  status: 0,
  name: null,
  tags: null,
  ip_address: null,
  imei: null,
};
