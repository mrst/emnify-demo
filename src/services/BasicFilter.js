import _ from 'lodash';

export class BasicFilter {
  constructor({ filterState }) {
    this.filterState = filterState;
  }

  isStateChanged(prevState) {
    return !_.isEqual(this.filterState, prevState);
  }

  updateState(stateToUpdate) {
    this.filterState = stateToUpdate;
  }

  mappedFilterValue() {
    return {
      status: this.filterState.status,
      name: this.filterState.name,
      tags: this.filterState.tags,
      ip_address: this.filterState.ip,
      imei: this.filterState.imei,
    };
  }
}
