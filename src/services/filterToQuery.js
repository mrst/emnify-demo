import _ from 'lodash';

export const filterToQuery = (queryParams) => {
  const filteredParams = _.omitBy(queryParams, _.isEmpty);
  const paramsArray = [];

  _.reduce(
    filteredParams, (result, value, key) => paramsArray.push(`${key}:${value}`), [],
  );

  return paramsArray.join(',');
};

export const paginationToQuery = (pagination) => {
  const paramsArray = [];

  _.reduce(
    pagination, (result, value, key) => paramsArray.push(`${key}:${value}`), [],
  );

  return paramsArray.toString().join(',');
};
