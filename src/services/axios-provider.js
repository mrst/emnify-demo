import axios from 'axios';

axios.defaults.withCredentials = true;

export const HTTP = axios.create({
  baseURL: process.env.VUE_APP_API_ENDPOINT,
  withCredentials: false,
});

export const setHeaders = (token) => {
  HTTP.defaults.headers.common['authorization'] = `Bearer ${token}`;
};
